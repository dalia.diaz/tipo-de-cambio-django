from ast import If
from distutils.log import error
from pickle import FALSE
from sqlite3 import IntegrityError
from django.shortcuts import render, redirect,get_object_or_404
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.contrib.auth import login,logout, authenticate
from django.db import IntegrityError
from .forms import TipoCambioForm
from .models import TipoCambio
from django.utils import timezone
from django.contrib.auth.decorators import login_required
import requests
import json
from django.core.mail import EmailMessage  
from django.contrib.sites.shortcuts import get_current_site  
from django.template.loader import render_to_string  
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode  
from django.utils.encoding import force_bytes, force_text  
from .token import account_activation_token  
from django.contrib.auth import get_user_model

# Create your views here.
# def helloword(request):
#     return HttpResponse('<h1>hello dalia<h1/>')

# Create your views here.
def home(request):
    return render(request, 'home.html')

def signup(request):
    if request.method == 'GET':
        print('enviando form')
        return render(request, 'signup.html',{
        'form':UserCreationForm
        })

    else:
        if request.POST['password1'] == request.POST['password2']:
            try:
                user = User.objects.create_user(username=request.POST['username'], password=request.POST['password1'], email=request.POST['email'])
                user.is_active=False
                user.save()
                # login(request,user)
                # to get the domain of the current site  
                current_site = get_current_site(request)  
                mail_subject = 'Activation link has been sent to your email id'  
                message = render_to_string('acc_active_email.html', {  
                    'user': user,  
                    'domain': current_site.domain,  
                    'uid':urlsafe_base64_encode(force_bytes(user.pk)),  
                    'token':account_activation_token.make_token(user),  
                })  
                to_email =request.POST['email']
                email = EmailMessage(  
                            mail_subject, message, to=[to_email]  
                )  
                email.send()  
                # return redirect('tipo_cambio')
                return HttpResponse('Please confirm your email address to complete the registration')  


            except IntegrityError:
                return render(request, 'signup.html',{
                    'form':UserCreationForm,
                    'error':'username already exists'
                })

        return render(request, 'signup.html',{
            'form':UserCreationForm,
            'error':'password do not match'
        })

@login_required
def signout(request):
    logout(request)
    return redirect('home')

def signin(request):
    if request.method=='GET':
        return render(request,'signin.html',{
        'form':AuthenticationForm
        })
    else:
        print(request.POST)
        user = authenticate(request, username=request.POST['username'], password=request.POST['password'])
        if user is None or user.is_active == False:
            return render(request,'signin.html',{
        'form':AuthenticationForm,
        'error':'username or password incorrect'
        })
        else:
            login(request,user)
            return redirect('tipo_cambio')    


@login_required   
def tipo_cambio(request):
    # return render(request,'tipo_cambio.html') 
    tipo=TipoCambio.objects.filter(user=request.user).order_by('fecha')
    
    # print(list(tipo.values()),'---')
    newListData=[]
    for d in list(tipo.values()):
        listData=json.loads(d['data'])
        amount=listData['query']['amount']
        result=listData['result']
        newData={
            "fecha":d['fecha'],
            "cantidad":d['cantidad'],
            "amount":amount,
            "result":"%.2f" % result,
            'id':d['id'],

        }
        newListData.append(newData)


    return render(request,'tipo_cambio.html',{'tipo':newListData}) 

@login_required
def agregarTipoCambio(request):
    if request.method == 'GET':
        return render(request,'agregar_tipo_cambio.html') 
    else:
        try:
            url = "https://api.apilayer.com/exchangerates_data/convert?to=GTQ&from=USD&amount="+request.POST['cantidad']+"&date="+request.POST['fecha']
            payload = {}
            headers= {
              "apikey": "CPdoISNiJYBdS21nRbZT0BSheDBWY6IX"
            }
            response = requests.request("GET", url, headers=headers, data = payload)
            status_code = response.status_code
            result = response.text
            print(result,'---------++++++++')

            form =TipoCambio(
                fecha=request.POST['fecha'],
                cantidad=request.POST['cantidad'],
                user=request.user,
                data=result
                )
            print(request.POST,'--------------')
            form.save()
            return redirect('tipo_cambio')
        except ValueError:
            return render(request,'agregar_tipo_cambio.html',{
                'error':'Please provide valida data'
                }) 
@login_required
def delete_type(request, type):
    type_doc = get_object_or_404(TipoCambio, pk=type, user=request.user)
    if request.method == 'POST':
        type_doc.delete()
        return redirect('tipo_cambio')

@login_required
def type_detail(request,type_id):
    if request.method == 'GET':
        type_doc = get_object_or_404(TipoCambio, pk=type_id, user=request.user)
        print(type_doc,'+++type_doc')
        form = TipoCambioForm(instance=type_doc)
        return render(request, 'type_detail.html', {'type_doc': type_doc,'form': form})
    else:
        try:
            type_doc = get_object_or_404(TipoCambio, pk=type_id, user=request.user)
            tipo=TipoCambio.objects.get(id=type_id)
            tipo.cantidad=request.POST['cantidad']
            tipo.fecha=request.POST['fecha']
            if type_doc.fecha != request.POST['fecha'] or type_doc.cantidad != request.POST['cantidad']:
                url = "https://api.apilayer.com/exchangerates_data/convert?to=GTQ&from=USD&amount="+request.POST['cantidad']+"&date="+request.POST['fecha']
                payload = {}
                headers= {
                  "apikey": "CPdoISNiJYBdS21nRbZT0BSheDBWY6IX"
                }
                response = requests.request("GET", url, headers=headers, data = payload)
                status_code = response.status_code
                result = response.text
                print('-------------here')
                tipo.data=result
                tipo.save()

            tipo.save()
            return redirect('tipo_cambio')
        except ValueError:
            return render(request, 'type_detail.html', {'type_doc': type_doc, 'form': form, 'error': 'Error updating type_doc.'})


def activate(request, uidb64, token):  
    User = get_user_model()  
    try:  
        uid = force_text(urlsafe_base64_decode(uidb64))  
        user = User.objects.get(pk=uid)  
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):  
        user = None  
    if user is not None and account_activation_token.check_token(user, token):  
        user.is_active = True  
        user.save()  
        return HttpResponse('Thank you for your email confirmation. Now you can login your account.')  
    else:  
        return HttpResponse('Activation link is invalid!') 