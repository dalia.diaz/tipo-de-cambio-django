from django.db import models
from django.contrib.auth.models import User
from django.contrib.postgres.fields.jsonb import JSONField

# Create your models here.

class TipoCambio(models.Model):
    fecha =models.TextField(max_length=10)
    data = models.TextField(max_length=400,null = True, blank = True)
    created=models.DateTimeField(auto_now_add=True)
    user=models.ForeignKey(User, on_delete=models.CASCADE)
    cantidad =models.TextField(max_length=10,null=True, blank=True)

    def __str__(self):
        return self.fecha + '- by '+ self.user.username
        